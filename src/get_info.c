#include <pebble.h>
#include "get_info.h"

#define ANIMATED true
#define NOT_ANIMATED false

static Window *get_info_window;

static TextLayer* text_layer;
static TextLayer* param_layers[] = { NULL, NULL, NULL };
static TextLayer* param_labels[] = { NULL, NULL, NULL };
static char* param_fields[] = { "Break Time", "Work Time", "Period Count" };
static int fieldModify = 0;
static char params_message[3][50];
static size_t PARAMS_MAX = 99;

static void itoa(int num, char* numStr)
{
    int i = 0;
    int temp_num = num;
    int length = 0;
    if(num >= 0) 
    {
        // count how many characters in the number
        while(temp_num) 
        {
            temp_num /= 10;
            length++;
        }
        // assign the number to the buffer starting at the end of the 
        // number and going to the begining since we are doing the
        // integer to character conversion on the last number in the
        // sequence
        for(i = 0; i < length; i++) 
        {
            numStr[(length-1)-i] = '0' + (num % 10);
            num /= 10;
        }
        numStr[i] = '\0'; // can't forget the null byte to properly end our string
    } else 
    { }
}

static void get_info_back_click_handler(ClickRecognizerRef recognizer, void *context) {
        window_stack_remove(get_info_window, true);
}

static void get_info_select_click_handler(ClickRecognizerRef recognizer, void *context) {
    int prevIndex = fieldModify;
    fieldModify = (fieldModify + 1) % 3;
    int newIndex = fieldModify;

    //Change old text layer to default colours
    text_layer_set_background_color( param_layers[prevIndex], GColorWhite );
    text_layer_set_text_color( param_layers[prevIndex], GColorBlack );

    //Highlight new selected text layer
    text_layer_set_background_color( param_layers[newIndex], GColorBlack );
    text_layer_set_text_color( param_layers[newIndex], GColorWhite );
}

static void get_info_up_click_handler(ClickRecognizerRef recognizer, void *context) {
    if ( param_values[fieldModify] < PARAMS_MAX ) {
        param_values[fieldModify] += 1;
        itoa(param_values[fieldModify], params_message[fieldModify]);
        text_layer_set_text( param_layers[fieldModify], params_message[fieldModify]);
    }
}

static void get_info_down_click_handler(ClickRecognizerRef recognizer, void *context) {
    if ( param_values[fieldModify] > 1 )
    {
        param_values[fieldModify] -= 1;
        itoa(param_values[fieldModify], params_message[fieldModify]);
        text_layer_set_text( param_layers[fieldModify], params_message[fieldModify]);
    }
}

static void get_info_window_load( Window* window)
{

    Layer* window_layer = window_get_root_layer( window );
    GRect window_bounds = layer_get_bounds( window_layer );


    text_layer = text_layer_create((GRect) { .origin = { 0, 126 }, .size = { window_bounds.size.w, 25 } });
    text_layer_set_text( text_layer, "Set your params!" );
    text_layer_set_text_alignment( text_layer, GTextAlignmentCenter );
    text_layer_set_background_color( text_layer, GColorWhite );

    layer_add_child( window_layer, text_layer_get_layer(text_layer));

    //init text_layers
    for (int i = 0; i < 3; i++) {
        TextLayer* label_layer = text_layer_create((GRect) { .origin = { (window_bounds.size.w/3) * i, (window_bounds.size.h/32) * 7 }, .size = { window_bounds.size.w/3, 28 } });
        text_layer_set_text( label_layer, param_fields[i]);
        text_layer_set_text_alignment( label_layer, GTextAlignmentCenter );
        layer_add_child( window_layer, text_layer_get_layer(label_layer));

        TextLayer* layer = text_layer_create((GRect) { .origin = { (window_bounds.size.w/3) * i , (window_bounds.size.h/16) * 7 }, .size = { window_bounds.size.w/3, 40 } });
        text_layer_set_font( layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD) );
        param_layers[i] = layer;

        text_layer_set_text_alignment( layer, GTextAlignmentCenter );
        if (i == fieldModify) {
            text_layer_set_background_color( layer, GColorBlack );
            text_layer_set_text_color( layer, GColorWhite );
        }
        else {
            text_layer_set_background_color( layer, GColorWhite );
            text_layer_set_text_color( layer, GColorBlack );
        }

        itoa(param_values[i], params_message[i]);
        text_layer_set_text( layer, params_message[i]);

        layer_add_child( window_layer, text_layer_get_layer(layer));
    }

    
}

static void get_info_window_unload( Window* window)
{
    text_layer_destroy(text_layer);
    for (int i = 0; i < 3; i++) {
        text_layer_destroy(param_layers[i]);
        text_layer_destroy(param_labels[i]);
    }
}

static void get_info_window_appear( Window* window)
{

}

static void get_info_window_disappear( Window* window)
{

}

void get_info_config_provider(Window *window) {
 // single click / repeat-on-hold config:
    window_single_click_subscribe(BUTTON_ID_SELECT, get_info_select_click_handler);
    window_single_click_subscribe(BUTTON_ID_UP, get_info_up_click_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN, get_info_down_click_handler);
    window_single_click_subscribe(BUTTON_ID_BACK, get_info_back_click_handler);

  //window_single_repeating_click_subscribe(BUTTON_ID_SELECT, 1000, select_single_click_handler);

  // multi click config:
  //window_multi_click_subscribe(BUTTON_ID_SELECT, 2, 10, 0, true, select_multi_click_handler);

  // long click config:
  //window_long_click_subscribe(BUTTON_ID_SELECT, 700, select_long_click_handler, select_long_click_release_handler);
}

void get_info_init() {
    //
    get_info_window = window_create();
    window_set_window_handlers( get_info_window, (WindowHandlers) {
        .load = get_info_window_load,
        .unload = get_info_window_unload,
        .appear = get_info_window_appear,
        .disappear = get_info_window_disappear } );
    window_set_click_config_provider(get_info_window, (ClickConfigProvider) get_info_config_provider);
    window_stack_push( get_info_window, ANIMATED );
}

void get_info_deinit() {
    window_destroy( get_info_window );
}