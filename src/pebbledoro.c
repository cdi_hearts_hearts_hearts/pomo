#include <pebble.h>
#include "get_info.h"

#define ANIMATED true
#define NOT_ANIMATED false

//#define _DEBUG

#ifdef _DEBUG
#define ATOMIC_POM_DURATION 2000 //milliseconds
#else
#define ATOMIC_POM_DURATION 30000 //30 seconds = 1/2 minutes
#endif

#define POMODORO_COUNT_PKEY 0

size_t param_values[3] = { 5, 25, 4 };
#define POMO_COUNT (param_values[1] * 2)
#define BREAK_COUNT (param_values[0] * 2)

static GFont FIRA_BOLD;

static Window *main_window;
static Window *menu_window;
static AppTimer* app_timer;
static TextLayer* text_layer;
static TextLayer* count_layer;
static char countStr[4];

static ActionBarLayer* actionbar_layer;

static SimpleMenuLayer *simple_menu_layer;
#define NUM_MENU_SECTIONS 1
static SimpleMenuSection menu_sections[NUM_MENU_SECTIONS];
#define NUM_FIRST_SECTION_ITEMS 1
static SimpleMenuItem first_section_items[NUM_FIRST_SECTION_ITEMS];

static GBitmap* action_icon_play;
static GBitmap* action_icon_stop;
static GBitmap* action_icon_stat;

static GRect bounds; 

static size_t atomic_pom_count;

static char pom_count_buf[100];

static enum POM_STATE {
    IN_PROGRESS,
    DONE,
    BREAK_IN_PROGRESS,
    BREAK_DONE,
} pomodoro_state;

void itoa(int num, char* numStr)
{
    int i = 0;
    int temp_num = num;
    int length = 0;
    if(num >= 0) 
    {
        // count how many characters in the number
        while(temp_num) 
        {
            temp_num /= 10;
            length++;
        }
        // assign the number to the buffer starting at the end of the 
        // number and going to the begining since we are doing the
        // integer to character conversion on the last number in the
        // sequence
        for(i = 0; i < length; i++) 
        {
            numStr[(length-1)-i] = '0' + (num % 10);
            num /= 10;
        }
        numStr[i] = '\0'; // can't forget the null byte to properly end our string
    } else 
    { }
}

static void action_bar_click_config_provider(void *context);

static void menu_select_callback(int index, void *ctx) {
    //window_stack_pop( true );
    static const uint32_t const segments[] = { 100, 100, 100 };
    VibePattern pat = {
        .durations = segments,
        .num_segments = ARRAY_LENGTH(segments),
    };
    vibes_enqueue_custom_pattern(pat);
}

//adds or removes a tick depending on pomodoro state
static void timer_callback( void* data )
{
    if ( pomodoro_state == IN_PROGRESS )
    {
        atomic_pom_count++;
        if ( atomic_pom_count % 2 == 0 ) {
            snprintf( countStr, 4, "%02d", (POMO_COUNT - atomic_pom_count)/2 );
            text_layer_set_text( count_layer, countStr );
        }
        if (atomic_pom_count == POMO_COUNT - 1) 
        {
            static const uint32_t const segments[] = { 100 };
            VibePattern pat = {
                .durations = segments,
                .num_segments = ARRAY_LENGTH(segments),
            };
            vibes_enqueue_custom_pattern(pat);   
        }
        if ( atomic_pom_count == POMO_COUNT )
        {
            static const uint32_t const segments[] = { 100, 100, 100 };
            VibePattern pat = {
                .durations = segments,
                .num_segments = ARRAY_LENGTH(segments),
            };
            vibes_enqueue_custom_pattern(pat);
            text_layer_set_text( text_layer, "Pomodoro finished!" );
            atomic_pom_count = 0;
            pomodoro_state = DONE;

            int32_t pom_count = persist_read_int( POMODORO_COUNT_PKEY );
            persist_write_int( POMODORO_COUNT_PKEY, pom_count + 1 );

            return;
        }
        app_timer = app_timer_register(ATOMIC_POM_DURATION, timer_callback, &atomic_pom_count);
    }
    else if ( pomodoro_state == BREAK_IN_PROGRESS ) //break
    {
        atomic_pom_count++;
        if ( atomic_pom_count % 2 == 0 ) {
            snprintf( countStr, 4, "%02d", (BREAK_COUNT - atomic_pom_count)/2 );
            text_layer_set_text( count_layer, countStr );
        }
        if (atomic_pom_count == BREAK_COUNT - 1 )
        {
            static const uint32_t const segments[] = { 100 };
            VibePattern pat = {
                .durations = segments,
                .num_segments = ARRAY_LENGTH(segments),
            };
            vibes_enqueue_custom_pattern(pat);   
        }
        if ( atomic_pom_count == BREAK_COUNT )
        {
            static const uint32_t const segments[] = { 100, 50, 200 };
            VibePattern pat = {
                .durations = segments,
                .num_segments = ARRAY_LENGTH(segments),
            };
            vibes_enqueue_custom_pattern(pat);
            text_layer_set_text( text_layer, "Break finished!" );
            pomodoro_state = BREAK_DONE;
            return;
        }
        app_timer = app_timer_register(ATOMIC_POM_DURATION, timer_callback, &atomic_pom_count);
    }
}

//middle button handler.  opens the stats menu
static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
    window_stack_push( menu_window, true );
}

// start button handler
static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
    if ( pomodoro_state == DONE ) //start break
    {
        snprintf( countStr, 4, "%02d", (BREAK_COUNT)/2 );
        text_layer_set_text( count_layer, countStr );
        app_timer = app_timer_register(ATOMIC_POM_DURATION, timer_callback, &atomic_pom_count);
        text_layer_set_text(text_layer, "Enjoy your break." );
        pomodoro_state = BREAK_IN_PROGRESS;
    }
    if ( pomodoro_state == BREAK_DONE ) //start pomodoro
    {
        atomic_pom_count = 0;
        snprintf( countStr, 4, "%02d", (POMO_COUNT)/2 );
        text_layer_set_text( count_layer, countStr );        
        app_timer = app_timer_register(ATOMIC_POM_DURATION, timer_callback, &atomic_pom_count);
        text_layer_set_text(text_layer, "Stay focused." );
        pomodoro_state = IN_PROGRESS;
    }
}

//cancels the pomodoro/break
static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
    if ( pomodoro_state == BREAK_IN_PROGRESS || pomodoro_state == DONE )
    {
        text_layer_set_text(text_layer, "Break cancelled." );
        pomodoro_state = BREAK_DONE;
    }
    else if ( pomodoro_state == IN_PROGRESS )
    {
        text_layer_set_text(text_layer, "Cancelled." );
        pomodoro_state = BREAK_DONE;
    }
}

static void action_bar_click_config_provider(void *context) {
    window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
    window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void window_load( Window* window )
{
    atomic_pom_count = 50;
    pomodoro_state = BREAK_DONE;
    Layer* window_layer = window_get_root_layer( window );
    GRect window_bounds = layer_get_bounds( window_layer );
    bounds = layer_get_bounds( window_layer );

    actionbar_layer = action_bar_layer_create();

    action_bar_layer_set_click_config_provider( actionbar_layer, action_bar_click_config_provider );
    action_bar_layer_set_icon( actionbar_layer, BUTTON_ID_UP, action_icon_play );
    action_bar_layer_set_icon( actionbar_layer, BUTTON_ID_DOWN, action_icon_stop );
    action_bar_layer_set_icon( actionbar_layer, BUTTON_ID_SELECT, action_icon_stat );

    //Message Box
    text_layer = text_layer_create((GRect) { .origin = { 0, 126 }, .size = { window_bounds.size.w - 22, 50 } });
    text_layer_set_text(text_layer, "Start a\npomodoro!" );
    text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
    text_layer_set_background_color( text_layer, GColorWhite );

    //Tick Box
    count_layer = text_layer_create((GRect) { .origin = { 0, window_bounds.size.h/9 }, .size = { window_bounds.size.w - 28, 90 } });
    text_layer_set_font( count_layer, FIRA_BOLD);
    text_layer_set_text(count_layer, "00" );
    text_layer_set_text_alignment(count_layer, GTextAlignmentCenter);
    text_layer_set_background_color( count_layer, GColorWhite );

    layer_add_child( window_layer, text_layer_get_layer(count_layer));

    layer_add_child( window_layer, text_layer_get_layer(text_layer));

    action_bar_layer_add_to_window( actionbar_layer, window );
}

static void window_unload( Window* window )
{
}

static void menu_window_unload( Window* window ) {

}

static void menu_window_appear( Window* window ) {
    Layer* window_layer = window_get_root_layer( window );

    int32_t pom_ct = persist_read_int( POMODORO_COUNT_PKEY );

    snprintf( pom_count_buf, 100, "%d", (int) pom_ct );

    //The simple menu layer
    first_section_items[ 0 ] = (SimpleMenuItem){
        .title = pom_count_buf,
        .subtitle = "Pomodoros completed",
        .callback = menu_select_callback,
    };

    menu_sections[ 0 ] = (SimpleMenuSection){
        .num_items = NUM_FIRST_SECTION_ITEMS,
        .items = first_section_items,
    };

    //layer_set_hidden( pomodoro_tick_layer, true );
    //Initialize the simple menu layer
    simple_menu_layer = simple_menu_layer_create( bounds, window, menu_sections, NUM_MENU_SECTIONS, NULL );
    // Add it to the window for display
    layer_add_child( window_layer, simple_menu_layer_get_layer( simple_menu_layer ) );
}

static void menu_window_load( Window* window ) {

}

static void menu_window_disappear( Window* window ) {
    layer_remove_from_parent( simple_menu_layer_get_layer( simple_menu_layer ) );
    simple_menu_layer_destroy( simple_menu_layer );
}

static void init( void )
{
    FIRA_BOLD = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_FIRA_BOLD_SUBSET_72));  
    
    if (!persist_exists( POMODORO_COUNT_PKEY ) ) persist_write_int( POMODORO_COUNT_PKEY, 0 );

    action_icon_play = gbitmap_create_with_resource( RESOURCE_ID_ICON_PLAY );
    action_icon_stop = gbitmap_create_with_resource( RESOURCE_ID_ICON_STOP );
    action_icon_stat = gbitmap_create_with_resource( RESOURCE_ID_ICON_STAT );

    main_window = window_create();
    window_set_window_handlers( main_window, (WindowHandlers) {
            .load = window_load,
            .unload = window_unload, } );

    menu_window = window_create();
    window_set_window_handlers( menu_window, (WindowHandlers) {
            .load = menu_window_load,
            .unload = menu_window_unload,
            .appear = menu_window_appear,
            .disappear = menu_window_disappear } );

    window_stack_push( main_window, NOT_ANIMATED );

    get_info_init();

}

static void deinit( void )
{
    get_info_deinit();
    window_destroy( menu_window );
    window_destroy( main_window );
}

int main( void ){
    init();
    app_event_loop();
    deinit();
}